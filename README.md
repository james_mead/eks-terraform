# AWS EKS Terraform Module

This Terraform module deploys an Amazon EKS (Elastic Kubernetes Service) cluster, complete with worker nodes, within your AWS account. It's designed to be flexible, allowing customization to fit a wide range of deployments.

## Prerequisites

Before you begin, ensure you have the following installed:

- [Terraform](https://www.terraform.io/downloads.html) (version 0.14 or later)
- [AWS CLI](https://aws.amazon.com/cli/) (configured with at least one profile)
- kubectl (for interacting with the cluster post-deployment)

## Quick Start

1. **Clone the repository**

    ```bash
    git clone git@gitlab.com:james_mead/eks-terraform.git
    cd eks-terraform.git
    ```

2. **Initialize Terraform**

    Initialize Terraform to download the necessary providers.

    ```bash
    terraform init
    ```

3. **Create a `terraform.tfvars` file**

    Copy the `terraform.tfvars.example` to `terraform.tfvars` and modify the values to match your setup.

    ```bash
    cp terraform.tfvars.example terraform.tfvars
    ```

4. **Plan the Deployment**

    Review the changes Terraform will perform.

    ```bash
    terraform plan
    ```

5. **Apply the Configuration**

    Apply the configuration to start the deployment.

    ```bash
    terraform apply
    ```

6. **Access Your Cluster**

    Once the deployment is complete, configure `kubectl` using the output credentials.

## Configuration

This project uses variables to allow for customization. See `variables.tf` for a detailed description of all variables.

### Key Variables

- `region`: The AWS region for the EKS cluster.
- `cluster_name`: The name of the EKS cluster.
- `node_group_name`: The name of the EKS node group.
- `subnet_ids`: List of subnet IDs for the EKS cluster.

For a full list of configurable variables, see `variables.tf`.

## Outputs

The module provides outputs that can be used to interact with the EKS cluster:

- `cluster_id`: The ID of the created EKS cluster.
- `cluster_endpoint`: The endpoint URL of the Kubernetes API server.
- `cluster_security_group_id`: The security group ID attached to the EKS cluster.

## Contributing

Contributions to this project are welcome! Please follow the guidelines in `CONTRIBUTING.md`.

## License

This project is licensed under the MIT License - see the `LICENSE` file for details.
