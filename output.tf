output "cluster_id" {
  description = "The ID of the EKS cluster."
  value       = aws_eks_cluster.cluster.id
}

output "cluster_endpoint" {
  description = "The endpoint for your EKS Kubernetes API."
  value       = aws_eks_cluster.cluster.endpoint
}

output "cluster_security_group_id" {
  description = "The security group ID attached to the EKS cluster."
  value       = aws_eks_cluster.cluster.vpc_config[0].cluster_security_group_id
}
