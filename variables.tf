variable "region" {
  description = "AWS region for the EKS cluster."
  type        = string
  default     = "us-west-2"
}
variable "profile" {
  description = "AWS Profile for the EKS Cluster"
  type        = string
  default     = "default"
  
}

variable "cluster_name" {
  description = "The name of the EKS cluster."
  type        = string
}

variable "node_group_name" {
  description = "The name of the EKS node group."
  type        = string
}

variable "subnet_ids" {
  description = "List of subnet IDs for the EKS cluster."
  type        = list(string)
}

variable "instance_types" {
  description = "Instance types for the worker nodes."
  type        = list(string)
  default     = ["t3.medium"]
}

variable "desired_capacity" {
  description = "Desired number of nodes in the node group."
  type        = number
}

variable "max_size" {
  description = "Maximum number of nodes in the node group."
  type        = number
}

variable "min_size" {
  description = "Minimum number of nodes in the node group."
  type        = number
}
